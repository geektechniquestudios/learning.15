package com.geektechnique.securitypractice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class UserSecConfig implements WebMvcConfigurer {

    //@Configuration//mvcconfigadapter is deprecated
    //protected static class ApplicationSecurity extends WebMvcConfigurerAdapter

//    @Autowired
//    DataSource dataSource;

    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication()
            .withUser("user1").password("{noop}user1").authorities("user_role").and()
            .withUser("user2").password("{noop}user2").authorities("user_role").and();
    }//BCryptPassword encoder for production

    //does not work - spent too much time - will look elsewhere for solutions
}
