package com.geektechnique.securitypractice;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;


//including spring boot security makes all urls secure by default
@RestController
public class SomeController {

    @GetMapping("/hello")
    public String hello(@RequestParam String name, Principal user){
        return "Test hello " + user.getName(); //gets name based who is logged in
    }

    //after logging in, http://localhost:8080/hello?name=terry
    //prints out Test hello Terry
    @GetMapping("/hello2")
    public String hello2(@RequestParam String name){
        return "Test hello " + name; //gets name based who is logged in
    }

    @GetMapping("hello3")
    public  String hello3(@RequestParam String name){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "Another test hello " + authentication.getName();//.getContext() is thread local

        //http://localhost:8080/hello3?name
    }


}
